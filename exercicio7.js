const val = "rain";
const birthDate = new Date("1995-11-28");
const momentDate = new Date("2004-01-18");
const idade = 27;

let datas = {
  name: "name1",
  isRain: val === "rain" ? true : false,
  birthDate: `${birthDate.getDate() + 1}-${
    birthDate.getMonth() + 1
  }-${birthDate.getFullYear()}`,
  momentDate: `${momentDate.getDate() + 1}-${
    momentDate.getMonth() + 1
  }-${momentDate.getFullYear()}`,
  age: idade + 1,
};

console.log(datas);
