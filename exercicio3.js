let birthDate = new Date("1995-11-28");
let momentDate = new Date("2004-01-18");

let birthDateFormatted = `${birthDate.getDate() + 1}-${birthDate.getMonth() + 1}-${birthDate.getFullYear()}`
let momentDateFormatted = `${momentDate.getDate() + 1}-${momentDate.getMonth() + 1}-${momentDate.getFullYear()}`

console.log(`${birthDateFormatted} / ${momentDateFormatted}`);
